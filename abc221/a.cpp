#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
  int a, b;
  cin >> a >> b;

  ll ans = 1;
  for (int i=0; i<(a-b); i++) ans *= 32;
  cout << ans << endl;
}
