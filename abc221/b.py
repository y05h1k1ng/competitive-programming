import copy

s = list(input())
t = list(input())

if s == t:
    print("Yes")
    exit()

for i in range(len(s)-1):
    tmp = copy.copy(s)
    tmp[i+1], tmp[i] = tmp[i], tmp[i+1]
    # print(tmp)
    if tmp == t:
        print("Yes")
        break
else:
    print("No")
