#include <bits/stdc++.h>
using namespace std;

using ll = long long;
const int HALF_T = 1e9+1;

ll d1[HALF_T], d2[HALF_T];

int main() {
  ll n;
  cin >> n;
  vector<ll> a(n), t(n);
  for (int i=0; i<n; i++) {
    int b;
    cin >> a[i] >> b;
    t[i] = a[i] + b;
  }

  for (int i=0; i<HALF_T; i++) {
    d1[i] = 0;
    d2[i] = 0;
  }

  for (int i=0; i<n; i++) {
    d1[a[i]]++;
    if (HALF_T < t[i]) d2[t[i]-HALF_T]--;
  }
  
  for (int i=0; i<HALF_T-1; i++) {
    d1[i+1] += d1[i];
  }
  for (int i=-1; i<HALF_T-1; i++) {
    if (i==-1) d2[i+1] += d1[HALF_T-1];
    else d2[i+1] += d2[i];
  }

  vector<ll> ans(n+1);
  for (int i=0; i<HALF_T; i++) {
    if (d1[i] == 0) continue;
    ans[d1[i]]++;
  }
  for (int i=0; i<HALF_T; i++) {
    if (d2[i] == 0) continue;
    ans[d2[i]]++;
  }

  for (int i=1; i<n+1; i++) {
    if (i == n) {
      cout << ans[i] << endl;
    } else {
      cout << ans[i] << " ";
    }
  }
  return 0;
}
