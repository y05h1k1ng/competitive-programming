from itertools import permutations

n = list(input())

ans = 0
for i in range(pow(2,len(n))):
    x_list = []
    y_list = []
    for idx in range(len(n)):
        if (i >> idx) & 1:
            x_list.append(n[idx])
        else:
            y_list.append(n[idx])
    if (len(x_list)==0) or (len(y_list)==0):
        continue
    for p in permutations(y_list, len(y_list)):
        y = "".join(p)
        if y[0] == "0":
            continue
        y = int(y)
        for q in permutations(x_list, len(x_list)):
            x = "".join(q)
            if x[0] == "0":
                continue
            x = int(x)
            ans = max(ans, x * y)
print(ans)
