#include <bits/stdc++.h>
using namespace std;

int main() {
  int n, m;
  cin >> n >> m;
  vector<vector<int>> to(n);
  for (int i=0; i<m; ++i) {
    int a, b;
    cin >> a >> b;
    --a; --b;
    to[a].push_back(b);
    to[b].push_back(a);
  }

  queue<int> que;
  vector<int> dist(n);
  que.push(0);
  dist[0] = 0;
  while (que.size()) {
    int v = que.front();
    que.pop();
    for (int nxt: to[v]) {
      if (dist[nxt] != 0) continue;
      que.push(nxt);
      dist[nxt] = dist[v] + 1;
    }
  }

  if (dist[n-1] == 2) {
    cout << "POSSIBLE" << endl;
  } else {
    cout << "IMPOSSIBLE" << endl;
  }
  return 0;
}
