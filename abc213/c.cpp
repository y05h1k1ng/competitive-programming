#include <bits/stdc++.h>
using namespace std;

int main() {
  int h, w, n;
  cin >> h >> w >> n;
  vector<int> A(n), B(n);
  for (int i=0; i<n; i++) {
    cin >> A[i] >> B[i];
  }

  vector<int> xs;
  for (int i=0; i<n; i++) xs.push_back(A[i]);
  sort(xs.begin(), xs.end());
  xs.erase(unique(xs.begin(), xs.end()), xs.end());
  for (int i=0; i<n; i++) {
    A[i] = lower_bound(xs.begin(), xs.end(), A[i]) - xs.begin();
  }

  vector<int> ys;
  for (int i=0; i<n; i++) ys.push_back(B[i]);
  sort(ys.begin(), ys.end());
  ys.erase(unique(ys.begin(), ys.end()), ys.end());
  for (int i=0; i<n; i++) {
    B[i] = lower_bound(ys.begin(), ys.end(), B[i]) - ys.begin();
  }

  for (int i=0; i<n; i++) {
    cout << A[i]+1 << " " << B[i]+1 << endl;
  }
}
