#include <bits/stdc++.h>
using namespace std;

int main() {
  int n, q;
  cin >> n >> q;
  vector<vector<int>> G(n);
  for (int i=0; i<n-1; ++i) {
    int a, b;
    cin >> a >> b;
    --a; --b;
    G[a].push_back(b);
    G[b].push_back(a);
  }

  queue<int> que;
  vector<int> dist(n, -1);
  que.push(0);
  dist[0] = 0;
  while (que.size()) {
    int t = que.front(); que.pop();
    for (int x: G[t]) {
      if (dist[x] == -1) {
        dist[x] = dist[t] + 1;
        que.push(x);
      }
    }
  }

  for (int i=0; i<q; ++i) {
    int c, d;
    cin >> c >> d;
    --c; --d;
    if ((dist[c]+dist[d])%2) {
      cout << "Road" << endl;
    } else {
      cout << "Town" << endl;
    }
  }
  return 0;
}
