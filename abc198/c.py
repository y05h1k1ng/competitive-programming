from math import sqrt, ceil

r, x, y = map(int, input().split())
d = sqrt(x**2 + y**2)
if d < r:
    print(2)
else:
    print(ceil(d / r))
