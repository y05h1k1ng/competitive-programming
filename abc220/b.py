k = int(input())
a, b = input().split()

def to_num(s, k):
    res = 0
    for i, c in enumerate(s[::-1]):
        res += int(c)*k**i
    return res

print(to_num(a, k) * to_num(b, k))
