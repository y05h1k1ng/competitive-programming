#include <bits/stdc++.h>
using namespace std;

using ll = long long;
const int MOD = 998244353;

int main() {
  int n;
  cin >> n;
  vector<int> a(n);
  for (int i=0; i<n; i++) cin >> a[i];

  vector<vector<ll>> dp(n, vector<ll>(10));
  dp[0][a[0]] = 1;
  for (int i=0; i<n-1; i++) {
    for (int j=0; j<10; j++) {
      int f, g;
      f = (a[i+1] + j) % 10;
      g = (a[i+1] * j) % 10;
      dp[i+1][f] += dp[i][j];
      dp[i+1][f] %= MOD;
      dp[i+1][g] += dp[i][j];
      dp[i+1][g] %= MOD;
    }
  }

  for (int i=0; i<10; i++) cout << dp[n-1][i] << endl;
  return 0;
}
