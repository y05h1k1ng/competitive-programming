#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
  int n;
  cin >> n;
  ll xx = 0;
  vector<ll> x(n);
  for (int i=0; i<n; i++) {
    ll a, b;
    cin >> a >> b;
    xx += a;
    x[i] = 2*a + b;
  }
  sort(x.rbegin(), x.rend());

  int ans = 0;
  while (xx >= 0) {
    xx -= x[ans];
    ans++;
  }
  cout << ans << endl;
  return 0;
}
