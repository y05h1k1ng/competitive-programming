n = int(input())

ws = []
for i in range(n):
    ws.append(input())

def finish(x):
    if x:
        print("WIN")
    else:
        print("LOSE")
    exit()

used = set()
prev = ""
for i, w in enumerate(ws):
    if (i != 0) and (prev[-1] != w[0]):
        finish(i&1)
    if w in used:
        finish(i&1)
    used.add(w)
    prev = w

print("DRAW")
