#include "bits/stdc++.h"
using namespace std;
using ll = long long;

int main() {
  int q;
  cin >> q;
  ll offset = 0;
  priority_queue<ll, vector<ll>, greater<ll>> que;
  for (int i=0; i<q; ++i) {
    int pre, x;
    cin >> pre;
    if (pre == 1) {
      cin >> x;
      que.push(x - offset);
    } else if (pre == 2) {
      cin >> x;
      offset += x;
    } else if (pre == 3) {
      ll ans = que.top() + offset;
      que.pop();
      cout << ans << endl;
    }
  }
  return 0;
}
