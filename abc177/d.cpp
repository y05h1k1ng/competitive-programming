#include <bits/stdc++.h>
using namespace std;
#include <atcoder/all>
using namespace atcoder;

int main() {
  int n, m;
  cin >> n >> m;
  dsu uf(n);
  for (int i=0; i<m; i++) {
    int a, b;
    cin >> a >> b;
    --a; --b;
    uf.merge(a, b);
  }

  int ans = 0;
  for (int i=0; i<n; i++) ans = max(ans, uf.size(i));
  cout << ans << endl;
  return 0;
}
