#include <bits/stdc++.h>
using namespace std;

int main() {
  int n;
  cin >> n;
  string s;
  cin >> s;

  int cw = 0, cr = 0;
  for (int i=0; i<n; i++) {
    if (s[i] == 'R') cr++;
    else cw++;
  }

  int ans = 0;
  for (int i=0; i<cr; i++) {
    if (s[i] == 'W') ans++;
  }

  cout << ans << endl;
  
  return 0;
}
