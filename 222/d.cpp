#include<bits/stdc++.h>
using namespace std;

const int MOD = 998244353;

int main() {
  int n;
  cin >> n;
  vector<int> a(n), b(n);
  for (int i=0; i<n; i++) cin >> a[i];
  for (int i=0; i<n; i++) cin >> b[i];


  vector<vector<int>> dp(n, vector<int>(3001));
  for (int i=0; i<3001; i++) {
    if (a[0] <= i && i <= b[0]) {
      dp[0][i] = 1;
    } else {
      dp[0][i] = 0;
    }
  }
  for (int i=0; i<n; i++) {
    int cnt = 0;
    for (int j=0; j<3001; j++) {
      if (dp[i][j]) {
        if (a[i+1] <= j && j <= b[i+1]) {
          dp[i+1][j] = dp[i][j] + cnt;
          dp[i+1][j] %= MOD;
          cnt++;
        } else {
          dp[i+1][j] = dp[i][j];
        }
      }
    }
  }

  int ans = 0;
  for (int i=0; i<3001; i++) {
    ans += dp[n-1][i];
    ans %= MOD;
  }
  cout << ans << endl;
  return 0;
}
