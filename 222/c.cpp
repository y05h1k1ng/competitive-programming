#include<bits/stdc++.h>
using namespace std;

using P = pair<int, int>;

bool compare_by_b(pair<int, int> a, pair<int, int> b) {
  if(a.first != b.first){
    return a.first > b.first;
  }else{
    return a.second < b.second;
  }
}

int main() {
  int n, m;
  cin >> n >> m;
  vector<string> a(2*n);
  for (int i=0; i<2*n; i++) cin >> a[i];

  vector<P> p(2*n);
  for (int i=0; i<2*n; i++) {
    p[i].first = 0;
    p[i].second = i;
  }
  
  for (int i=0; i<m; i++) { // round
    for (int j=0; j<n; j++) {
      char x = a[p[2*j].second][i];
      char y = a[p[2*j+1].second][i];
      if (x == y) {
        // aiko
        // cout << "[*] aiko " << i << " " <<  j << endl;
      } else if((x == 'G' && y == 'C') || (x == 'C' && y == 'P') || (x == 'P' && y == 'G')) {
        // win x;
        // cout << "[*] win x " << i << " " <<  j << endl;
        p[2*j].first++;
      } else if((y == 'G' && x == 'C') || (y == 'C' && x == 'P') || (y == 'P' && x == 'G')) {
        // win y;
        // cout << "[*] win y " << i << " " <<  j << endl;
        p[2*j+1].first++;
      } else {
        cout << "[*] ??" << endl;
      }
    }
    sort(p.begin(), p.end(), compare_by_b);
    // printf("[*] %d: ", i);
    // for (int i=0; i<2*n; i++) cout  << p[i].second << ":" << p[i].first << " ";
    // printf("\n");
  }

  vector<int> ans(2*n);
  for (int i=0; i<2*n; i++) {
    cout << p[i].second+1 << endl;
  }
}
