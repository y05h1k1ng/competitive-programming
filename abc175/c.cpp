#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
  ll x, k, d;
  cin >> x >> k >> d;

  x = abs(x);
  ll i = x / d;

  ll ans = 0;
  if (i > k) {
    ans = x - k*d;
  } else {
    ll j = k - i;
    if (j % 2 == 0) {
      ans = x - i*d;
    } else {
      ans = x - (i+1) * d;
      ans = abs(ans);
    }
  }
  cout << ans << endl;
  return 0;
}
