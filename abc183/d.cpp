#include <bits/stdc++.h>
using namespace std;

using ll = long long;
const int MAX_T = 200005;

int main() {
  int n, w;
  cin >> n >> w;
  vector<ll> d(MAX_T);
  for (int i=0; i<n; i++) {
    int s, t, p;
    cin >> s >> t >> p;
    d[s] += p;
    d[t] -= p;
  }
  for (int i=0; i<MAX_T-1; i++) {
    d[i+1] += d[i];
  }
  for (int i=0; i<MAX_T; i++) {
    if (d[i] > w) {
      cout << "No" << endl;
      return 0;
    }
  }
  cout << "Yes" << endl;
  return 0;
}
