#include <bits/stdc++.h>
using namespace std;

int main() {
  string s;
  cin >> s;

  int ans = 0;
  for (int num=0; num<10000; ++num) {
    vector<int> tb(10);
    int x = num;
    for (int i=0; i<4; ++i) {
      int d = x%10;
      tb[d] = 1;
      x /= 10;
    }

    bool ok = true;
    for (int i=0; i<10; ++i) {
      if (s[i] == 'o' && tb[i] != 1) ok = false;
      if (s[i] == 'x' && tb[i] != 0) ok = false;
    }
    if (ok) ++ans;
  }
  cout << ans << endl;
  return 0;
}
