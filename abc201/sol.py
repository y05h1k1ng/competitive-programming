s = input()

table = [c=="o" for c in s]

def check(p):
    tb = [False] * 10
    for i in range(4):
        if s[int(p[i])] == "x":
            return False
        if s[int(p[i])] == "o":
            tb[int(p[i])] = True
    if tb == table:
        return True
    else:
        return False

ans = 0
for i in range(0, 10**4):
    password = str(i).zfill(4)
    if check(password):
        ans += 1

print(ans)
