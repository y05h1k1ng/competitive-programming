#include <bits/stdc++.h>
using namespace std;

int main() {
  int h, w, k;
  cin >> h >> w >> k;
  vector<string> s(h);
  for (int i=0; i<h; i++) cin >> s[i];

  int ans = 0;
  for (int is=0; is<(1<<h); is++) {
    for (int js=0; js<(1<<w); js++) {
      int cnt = 0;
      for (int x=0; x<h; x++)  for (int y=0; y<w; y++) {
          if (is>>x&1) continue;
          if (js>>y&1) continue;
          if (s[x][y] == '#') cnt++;
        }
      if (cnt == k) ans++;
    }
  }
  cout << ans << endl;
  return 0;
}
