#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
  int n;
  cin >> n;
  vector<ll> a(n);
  for (int i=0; i<n; ++i) cin >> a[i];

  for (int i=0; i<n; ++i) a[i] *= -1;
  sort(a.begin(), a.end());
  for (int i=0; i<n; ++i) a[i] *= -1;

  vector<ll> b(2*n);
  for (int i=0; i<n; ++i) {
    b[2*i] = a[i];
    b[2*i+1] = a[i];
  }

  if (n == 1) {
    cout << 0 << endl;
  } else {
    ll ans = 0;
    for (int i=1; i<n; i++) ans += b[i];
    cout << ans << endl;
  }
  return 0;
}
