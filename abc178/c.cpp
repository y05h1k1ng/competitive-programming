#include <bits/stdc++.h>
using namespace std;

using ll = long long;
const int MOD = 1e9 + 7;

ll powmod(ll x, ll y) {
  ll res = 1;
  for (ll i=0; i<y; i++) {
    res *= x;
    res %= MOD;
  }
  return res;
}

int main() {
  int n;
  cin >> n;

  ll ans = powmod(10, n) - powmod(9, n) - powmod(9, n) + powmod(8, n);
  ans %= MOD;
  ans = (ans + MOD) % MOD;
  cout << ans << endl;
  return 0;
}
