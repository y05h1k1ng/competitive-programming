#include <bits/stdc++.h>
using namespace std;

int n;
int ans = 100;
vector<int> a;

// https://kamino.hatenablog.com/entry/cpp-permutation-combination
void recursive_comb(int *indexes, int s, int rest, std::function<void(int *)> f) {
  if (rest == 0) {
    f(indexes);
  } else {
    if (s < 0) return;
    recursive_comb(indexes, s - 1, rest, f);
    indexes[rest - 1] = s;
    recursive_comb(indexes, s - 1, rest - 1, f);
  }
}

// nCkの組み合わせに対して処理を実行する
void foreach_comb(int n, int k, std::function<void(int *)> f) {
  int indexes[k];
  recursive_comb(indexes, n - 1, k, f);
}

int main() {
  int c;
  cin >> n >> c;
  for (int i=0; i<n; ++i) {
    int x;
    cin >> x;
    --x;
    a.push_back(x);
  }



  foreach_comb(10, 2, [](int *indexes) {
    int num1, num2, ch;
    num1 = indexes[0];
    num2 = indexes[1];
    ch = 0;
    
    // num1, num2, num1, ...
    for (int i=0; i<n; ++i) {
      if ((i%2)==0) {
        if (a[i] != num1) ++ch;
      } else {
        if (a[i] != num2) ++ch;
      }
    }

    ans = min(ans, ch);
    ch = 0;

    // num2, num1, num2, ...
    for (int i=0; i<n; ++i) {
      if ((i%2)==0) {
        if (a[i] != num2) ++ch;
      } else {
        if (a[i] != num1) ++ch;
      }
    }
    ans = min(ans, ch);
  });

  cout << ans * c << endl;
  return 0;
}
