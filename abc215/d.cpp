#include <bits/stdc++.h>
using namespace std;

bool NG[101010];

vector<int> factor(int x) {
  vector<int> res;
  for(int i=2; i*i<=x; ++i) {
    while(x%i == 0) {
      x /= i;
      res.push_back(i);
    }
  }
  if(x != 1) {
    res.push_back(x);
  }
  return res;
}

int main() {
  int n, m;
  cin >> n >> m;
  vector<int> A(n);
  for(int i=0; i<n; ++i) {
    cin >> A[i];
  }
  
  for(int i=0; i<n; ++i) {
    auto ep = factor(A[i]);
    for(const auto &p: ep) {
      NG[p] = true;
    }
  }

  for(int p=2; p<m; ++p) {
    if(NG[p]) {
      for(int y=p+p; y<=m; y+=p) {
        NG[y] = true;
      }
    }
  }

  vector<int> ans;
  for(int i=1; i<=m; ++i) {
    if(!NG[i]) ans.push_back(i);
  }
  
  cout << ans.size() << endl;
  for(const auto &p: ans) {
    cout << p << endl;
  }
  return 0;
}
