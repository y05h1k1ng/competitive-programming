#include <bits/stdc++.h>
using namespace std;

using ll = long long;

tuple<ll, ll, ll> extgcd(ll a, ll b) {
  if (b == 0) return {a, 1, 0};
  ll g, x, y;
  tie(g, x, y) = extgcd(b, a%b);
  return {g, y, x-a/b*y};
}

void solve() {
  ll n, s, k;
  cin >> n >> s >> k;
  ll g, x, y;
  tie(g, x, y) = extgcd(k, n);
  if (s%g != 0) {
    cout << -1 << endl;
    return;
  }
  n /= g;
  s /= g;
  k /= g;
  ll ans = ((x*-s)%n + n)%n;
  cout << ans << endl;
}

int main() {
  int t;
  cin >> t;
  for (int i=0; i<t; i++) {
    solve();
  }
  return 0;
}
