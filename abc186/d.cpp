#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
  int n;
  cin >> n;
  vector<int> a(n);
  for (int i=0; i<n; i++) {
    cin >> a[i];
  }

  sort(a.begin(), a.end());

  ll s = 0;
  ll ans = 0;
  for (int j=0; j<n; j++) {
    ans += (ll)a[j]*j;
    ans -= s;
    s += a[j];
  }

  cout << ans << endl;
  return 0;
}
