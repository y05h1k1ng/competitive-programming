#include <bits/stdc++.h>
using namespace std;

int main() {
  string s;
  cin >> s;
  int n = s.size();
  vector<vector<int>> dp(n+1, vector<int>(9));
  for (int i=0; i<=n; ++i) {
    dp[i][0] = 1;
  }
  const int mod = 1000000007;
  string t = "chokudai";
  for (int i=1; i<=n; i++) {
    for (int j=1; j<9; j++) {
      if (s[i-1] != t[j-1]) {
        dp[i][j] = dp[i-1][j];
      } else {
        dp[i][j] = (dp[i-1][j] + dp[i-1][j-1]) % mod; 
      }
    }
  }
  cout << dp[n][8] << endl;
  return 0;
}
