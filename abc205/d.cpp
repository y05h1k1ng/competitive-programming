#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
  int n, q;
  cin >> n >> q;
  vector<ll> A(n);
  for (int i=0; i<n; ++i) cin >> A[i];

  vector<ll> c(n);
  for (int i=0; i<n; ++i) {
    c[i] = A[i] - (i+1);
  }
  
  for (int i=0; i<q; ++i) {
    ll k, ans;
    cin >> k;
    int pos = lower_bound(c.begin(), c.end(), k) - c.begin();
    if (pos == 0) {
      ans = k;
    } else {
      ans = A[pos-1] + (k-c[pos-1]);
    }
    cout << ans << endl;
  }
  return 0;
}
