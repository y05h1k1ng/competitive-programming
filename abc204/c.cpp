#include <bits/stdc++.h>
using namespace std;

int main() {
  int n, m;
  cin >> n >> m;
  vector<vector<int>> to(n);
  for (int i=0; i<m; ++i) {
    int a, b;
    cin >> a >> b;
    --a; --b;
    to[a].push_back(b);
  }

  int ans = 0;
  for (int i=0; i<n; ++i) {
    queue<int> que;
    vector<bool> visited(n);
    que.push(i); ++ans;
    visited[i] = true;
    while (que.size()) {
      int x = que.front(); que.pop();
      for (int nxt: to[x]) {
        if (visited[nxt]) continue;
        que.push(nxt); ++ans;
        visited[nxt] = true;
      }
    }
  }
  cout << ans << endl;
  return 0;
}
