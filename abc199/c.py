n = int(input())
s = input()
q = int(input())

isFlip = False
ans = list(s)
for _ in range(q):
    t, a, b = map(int, input().split())
    a -= 1
    b -= 1
    if t == 1:
        if isFlip:
            if n <= a and n <= b:
                ans[a-n], ans[b-n] = ans[b-n], ans[a-n]
            elif n <= a and b < n:
                ans[a-n], ans[b+n] = ans[b+n], ans[a-n]
            elif a < n and n <= b:
                ans[a+n], ans[b-n] = ans[b-n], ans[a+n]
            else:
                ans[a+n], ans[b+n] = ans[b+n], ans[a+n]
        else:
            ans[a], ans[b] = ans[b], ans[a]
    else:
        isFlip = not isFlip
    # print("[*]", isFlip, "".join(ans))
if isFlip:
    ans = ans[n:] + ans[:n]
print("".join(ans))
