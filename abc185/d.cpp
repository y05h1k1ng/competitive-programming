#include <bits/stdc++.h>
using namespace std;

int main() {
  int n, m;
  cin >> n >> m;
  vector<int> a(m+2);
  a[0] = 0;
  a[m+1] = n+1;
  for (int i=0; i<m; i++) cin >> a[i];

  sort(a.begin(), a.end());
  vector<int> d;
  int k = n+5;
  for (int i=0; i<m+1; i++) {
    int dist = a[i+1] - a[i] - 1;
    if (dist == 0) continue;
    d.push_back(dist);
    k = min(k, dist);
  }

  int ans = 0;
  for (auto dist: d) {
    ans += (dist + k-1) / k;
  }
  cout << ans << endl;
  return 0;
}
