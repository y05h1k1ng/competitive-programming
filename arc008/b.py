n, m = map(int, input().split())

name = input()
kit = input()

# todo: 含まれてないと不可能check
if set(name) - set(kit):
    print(-1)
    exit()

used = [False] * n
ans = 0
for _ in range(n):
    ans += 1
    for i in range(m):
        for j in range(n):
            if (not used[j]) and (kit[i] == name[j]):
                used[j] = True
                break
    if all(used):
        break

print(ans)
