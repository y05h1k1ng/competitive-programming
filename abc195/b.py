a, b, w = map(int, input().split())

w *= 1000

ans = []
for x in range(1, w+1):
    if a*x <= w <= b*x:
        ans.append(x)

if ans:
    print(min(ans), max(ans))
else:
    print("UNSATISFIABLE")
