#include <bits/stdc++.h>
using namespace std;

int main() {
  int n, m, k;
  cin >> n >> m;
  vector<int> a(m), b(m);
  for (int i=0; i<m; ++i) {
    cin >> a[i] >> b[i];
  }
  cin >> k;
  vector<int> c(k), d(k);
  for (int i=0; i<k; ++i) {
    cin >> c[i] >> d[i];
  }

  int ans = 0;
  for (int i=0; i<(2<<k); ++i) {
    vector<int> table(n+1);
    for (int j=0; j<k; ++j) {
      if ((i>>j) & 1) {
        table[d[j]] = 1;
      } else {
        table[c[j]] = 1;
      }
    }

    int res = 0;
    for (int j=0; j<m; ++j) {
      if (table[a[j]] == 1 && table[b[j]] == 1) {
        ++res;
      }
    }
    ans = max(ans, res);
  }
  cout << ans << endl;
  return 0;
}
